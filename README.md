### To Setup the project you have to clone this repo 
    https://gitlab.com/naveengupta91/book-store.git
    Install python3 
    Install Virtual envirenment 
    Python -m venv venv 
    Install requirements.txt file 
    pip install -r requirements.txt


###  To start the project on Staging
    python manage.py runserver --settings=Book_Store.settings.staging

###  To makemigrations the models
    python manage.py makemigrations --settings=Book_Store.settings.staging

###  To migrate the models
    python manage.py migrate --settings=Book_Store.settings.staging


###  To start the project on Produciton
    python manage.py runserver --settings=Book_Store.settings.prod

###  To makemigrations the models on Production
    python manage.py makemigrations --settings=Book_Store.settings.prod

###  To migrate the models on Production
    python manage.py migrate --settings=Book_Store.settings.prod


###  API's -
    1-  POST http://127.0.0.1:8000/user_auth/register/
        Description: User can register passing email and other details and get otp on email to email verificaiton
        Body:{
            "first_name": "Nilkam",
            "last_name": "Sahu",
            "password": "212",
            "email": "nilkaml@gmail.com",
            "profile_pic": null,
            "phone_number": "893489343434"
            }
        Response: {"message": Successfully register please check email for OTP verify"}

    2-  POST http://127.0.0.1:8000/user_auth/otp_verify/
        Description: Here after registration user get otp on email to verify

        Body: {
            "otp": 3994,
            "email": "nknaveengupa@gmail.com"
            }
        Response: 

    3-  POST http://127.0.0.1:8000/user_auth/login/
        Description: Here user can user Email and Password to login If credentials right then user successfully logged in

        Body:{
            "email": "nilkaml@gmail.com",
            "password": "212"
            }
        Response: 

    4-  POST http://127.0.0.1:8000/user_auth/forget_password/
        Description: user can give request to forget password using Email
        Body: {
            "email": "nilkaml@gmail.com"
            }
        Response: 

    5-  POST http://127.0.0.1:8000/user_auth/reset_password/
        Description: User can reset forget password using email and OTP verify
        Body:{
            "otp": 9305,
            "password": "987mn"
            }
        Response: 
    
    6-  GET http://127.0.0.1:8000/book/search/?search=python

        Description: Search api , Here user can search any books using book title, book auther name, book id.
                    if books does not match then fetch data from google book api
        Header = {
            "HTTP_LIMIT": 0
            }
        Request Params
        search - python

    7-  GET http://127.0.0.1:8000/book/booklist/?user_id=12&book_list=READLIST

        Description: Here user can see list of books from three categoryis like readlist, 
                    wishlist and selflist parameter required- user-id, book_list

        Request Params
        user_id - 12
        book_list - READLIST
    
    8-  GET http://127.0.0.1:8000/user_auth/user_profile/?user_id=12
        Description: 
        Request Params
        user_id - 12

    9-  PUT http://127.0.0.1:8000/user_auth/user_profile/?user_id=12

        Description: User can update own profile and edit it

        Request Params
        user_id - 12
        Body:{
            "first_name": "Sivani",
            "last_name": "Sahu",
            "otp": "6008",
            "password": "212",
            "email": "shivani@gmail.com",
            "profile_pic": null,
            "phone_number": 8934893434
        }
    
    10- POST http://127.0.0.1:8000/book/booklist/

        Description: User can add your favoriate books in readlist, wishlista and selflist

        Body:{
            "book": 59,
            "user": 12
            }

        Response: 
    
    11- POST http://127.0.0.1:8000/book/rating/

        Description: user can save rating and review of books

        Body:{
            "rating": 3,
            "review": "its a Nice ",
            "user": 12,
            "book": 54
            }

        Response: 

    12- GET http://127.0.0.1:8000/book/rating/

        Description: show top 10 rated books

        Response: 

    13- GET http://127.0.0.1:8000/book/single_book/?book_id=47&user_id=11
        
        Description:  Here User can see full details of single books
        
        Request Params
        book_id - 47
        user_id - 11

        Response: 