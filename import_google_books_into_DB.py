import json
import sys
import json
from googleapiclient.discovery import build

API_KEY = "AIzaSyBrCE9x7nKlN0SfS54WiqeQOyN6nxaHgZA"


def getGoogleBooks(search):
    service = build('books', 'v1', developerKey=API_KEY)
    request = service.volumes().list(maxResults=40,
                                     filter='full', q={search})
    response = request.execute()
    # print(json.dumps(response, sort_keys=True, indent=4))
    # print("Number of books in list:", len(response['items']))

    for book in response.get('items', []):
        json_data = {
            'title': book['volumeInfo']['title'],
            'book_id': book['id'],
            'language': book['volumeInfo']['language'],
            'infoLink': book['volumeInfo']['infoLink'],
            'imageLinks': book['volumeInfo']['imageLinks']['thumbnail'],
            'categories': book['volumeInfo']['categories'][0],
            'volumeInfo': book['volumeInfo']['canonicalVolumeLink'],
            'book_type': book['kind'],
            'webReaderLink': book['accessInfo']['webReaderLink'],
            'downloadLink': book['accessInfo']['pdf']['downloadLink'],
            
           }


getGoogleBooks(search="Python")
