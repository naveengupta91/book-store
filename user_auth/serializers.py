from django.core.exceptions import ValidationError
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.core.validators import validate_email
from .models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'otp', 'password', 'email', 
                  'profile_pic', 'phone_number', 'created_at',
                  'updated_at')
        extra_kwargs = {'email': {'required': True, 'allow_blank': False},
                        'password': {'required': True, 'allow_blank': False}}

