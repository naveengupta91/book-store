from django.db import models

# Create your models here.
from django.contrib.auth.models import UserManager, AbstractUser
from django.db import models

# Create your models here.


class User(AbstractUser):
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=40, unique=False, default='')
    first_name = models.CharField(max_length=150, blank=True)
    last_name = models.CharField(max_length=150, blank=True)
    password = models.CharField(max_length=500, blank=False)
    email = models.EmailField(max_length=255, unique=True)
    profile_pic = models.URLField(blank=True, null=True)
    phone_number = models.CharField(max_length=15, blank=True, null=True,
                                    unique=True)
    otp = models.CharField(max_length=10, blank=True, null=True)
    isVerified = models.BooleanField(blank=False, default=False)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = [
        "username",
    ]


