from user_auth.serializers import UserSerializer
from django.urls import path
from user_auth.views import RegisterView, OTPVerify,\
 Login, ForgetPassword, ResetPassword, UserProfileView


urlpatterns = [
    path('register/', RegisterView.as_view(), name='auth_register'),
    path('otp_verify/', OTPVerify.as_view(), name='otp_verify'),
    path('login/', Login.as_view(), name='login'),
    path('forget_password/', ForgetPassword.as_view(), name='forget_password'),
    path('reset_password/', ResetPassword.as_view(), name='reset_password'),
    path('user_profile/', UserProfileView.as_view(), name='user_profile'),
]
