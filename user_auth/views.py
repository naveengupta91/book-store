# Python
import math
import random

# Django Module
from django.core.mail import send_mail
from django.core.exceptions import ObjectDoesNotExist

# Django_rest_framework module
from rest_framework.views import APIView
from .models import User
from .serializers import UserSerializer
from rest_framework.response import Response
from rest_framework import status


class UserProfileView(APIView):

    def get_object(self, user_id):
        '''
        Helper method to get the object with given todo_id, and user_id
        '''
        try:

            return User.objects.get(id=user_id)
        except ObjectDoesNotExist:
            return None

    def get(self, request, *args, **kwargs):
        user_id = request.GET.get('user_id')
        user_instance = self.get_object(user_id)
        if user_instance:
            serializer = UserSerializer(user_instance)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response({"res": "User does not exists"},
                        status=status.HTTP_404_NOT_FOUND)

    def put(self, request, format=None):
        user_id = request.GET.get('user_id')
        user_instance = self.get_object(user_id)
        if user_instance:
            serializer = UserSerializer(user_instance, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        return Response({'res': "Please pass user id to udpate user profile "},
                        status=status.HTTP_400_BAD_REQUEST)


class RegisterView(APIView):
    """
        In this api User can create account usign email and verify using OTP 
        given on user Email if OTP does not match it return
        Please try again and check you email

    """

    def post(self, request, *args, **kwargs):
        print(request.data)
        email = request.data.get("email")
        serializer = UserSerializer(data=request.data)

        if serializer.is_valid():
            try:
                user_instance = User.objects.get(email=email)
            except ObjectDoesNotExist:
                user_instance = None
            if not user_instance:
                otp = send_otp(email)
                serializer.save(otp=otp)
                return Response(serializer.data,
                                status=status.HTTP_201_CREATED)
            return Response({"res": "User already exist"},
                            status=status.HTTP_409_CONFLICT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OTPVerify(APIView):

    def get_object(self, email):
        '''
        Helper method to get the object with given todo_id, and user_id
        '''
        try:

            return User.objects.get(email=email)
        except ObjectDoesNotExist:
            return None

    def post(self, request, *args, **kwargs):
        '''
        Retrieves the user data using user id to match otp
        '''
        user_otp = request.data.get("otp")
        missing_field = "OTP" if not user_otp else False
        if missing_field:
            return Response({"res": f"Kindly pass {missing_field}"}, 
                            status=status.HTTP_400_BAD_REQUEST)

        user_instance = self.get_object(request.data.get('email'))
        if not user_instance:
            return Response(
                {"res": "User email does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )
        db_otp = user_instance.otp
        if int(user_otp) == int(db_otp):
            return Response({"message": "OTP Verified"},
                            status=status.HTTP_200_OK)
        return Response({"message": "Wrong OTP please check "},
                        status=status.HTTP_400_BAD_REQUEST)


class Login(APIView):

    def get_object(self, email, password):
        '''
        Helper method to get the user email and passwrod data to match 
        its authenticate user or not

        '''
        try:

            return User.objects.get(email=email, password=password)
        except ObjectDoesNotExist:
            return None

    def post(self, request, *args, **kwargs):
        """
         login using email and password if email and password correct user 
         logged in
        :param request: email, password
        :param args:
        :param kwargs:
        :return: login successfully
        """
        email = request.data.get("email")
        password = request.data.get("password")
        missing_field = "email" if not email else "password" if not password else False
        if missing_field:
            return Response({"res": f"Kindly pass {missing_field}"},
                            status=status.HTTP_400_BAD_REQUEST)

        user_instance = self.get_object(email,  password)
        if not user_instance:
            return Response(
                {"res": "wrong email or password please check"},
                status=status.HTTP_400_BAD_REQUEST
            )

        return Response({"res": "Login successfully"}, 
                        status=status.HTTP_200_OK)


class ForgetPassword(APIView):
    """
        With the help of this api user can request to reset the password 
        using OTP
        :param email
        :return User does not exist
        :return OTP generated successfully
    """
    def post(self, request, *args, **kwargs):
        email = request.data.get("email")
        missing_field = "email" if not email else False
        if missing_field:
            return Response({"res": f"Kindly pass {missing_field}"},
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            user_instance = User.objects.get(email=email)
        except ObjectDoesNotExist:
            user_instance = None

        if not user_instance:
            return Response(
                {"res": "User does not exists"},
                status=status.HTTP_404_NOT_FOUND
            )
        otp = send_otp(email)
        user_instance.otp = otp
        user_instance.save()
        return Response({"res": "OTP Generated successfully"},
                        status=status.HTTP_200_OK)


class ResetPassword(APIView):
    """
        User can reset the password when otp matched
        :param otp
        :param password
        :return successfully reset pass
        :return Wrong OTP
    """

    def post(self, request, *args, **kwargs):
        otp = request.data.get("otp")
        password = request.data.get("password")
        missing_field = "OTP" if not otp else "password" if not password else False
        if missing_field:
            return Response({"res": f"Kindly pass {missing_field}"},
                            status=status.HTTP_400_BAD_REQUEST)

        try:
            user_instance = User.objects.get(otp=otp)
        except ObjectDoesNotExist:
            user_instance = None

        if not user_instance:
            return Response(
                {"res": "Wrong OTP please try again"},
                status=status.HTTP_404_NOT_FOUND
            )

        db_otop = user_instance.otp
        if int(db_otop) == int(otp):
            user_instance.password = password
            user_instance.save()
            return Response({"res": "Password reset successfully"},
                            status=status.HTTP_200_OK)


def generateOTP():
    digits = "0123456789"
    OTP = ""
    for i in range(4):
        OTP += digits[math.floor(random.random() * 10)]
    return OTP


def send_otp(email):
    """
     It create a unique 4 digit number and send via email
    :param email:
    :return: otp
    """
    try:
        print(email)
        otp = generateOTP()
        print(otp)
        htmlgen = f'<p>Your OTP is <strong>{otp}</strong></p>'
        send_mail('OTP request', otp, '<your gmail id>', [email],
                  fail_silently=False, html_message=htmlgen)
    except Exception as e:
        print(e)
        otp = None
    return otp