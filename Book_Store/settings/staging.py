"""Use this for development"""

from .base import *

ALLOWED_HOSTS += ['*']
DEBUG = True

WSGI_APPLICATION = 'Book_Store.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'bookstore',
        'USER': 'developer',
        'HOST': 'localhost',
        'PORT': '',
    }
}