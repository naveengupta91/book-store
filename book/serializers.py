from user_auth.serializers import UserSerializer
from django.db import models
from django.db.models import fields
from rest_framework import serializers
from .models import Book, BookReadList, Rating


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('book_id', 'title', 'authors', 'publisher', 'publishedDate',
                  'language', 'infoLink', 'imageLinks',
                  'categories', 'volumeInfo', 'book_type', 'webReaderLink',
                  'description')


class ReadRatingSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    book = BookSerializer()

    class Meta:
        model = Rating
        fields = ('rating', 'review', 'user', 'book', 'created_at',
                  'updated_at')


class WriteRatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rating
        fields = ('rating', 'review', 'user', 'book', 'created_at',
                  'updated_at')


class ReadBookListSerializer(serializers.ModelSerializer):
    book = BookSerializer()
    user = UserSerializer()

    class Meta:
        model = BookReadList
        fields = ('book', 'user', 'booklist', 'created_at', 'updated_at')


class WriteBookListSerializer(serializers.ModelSerializer):

    class Meta:
        model = BookReadList
        fields = ('book', 'user', 'booklist', 'created_at', 'updated_at')