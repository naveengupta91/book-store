from django.db import models
from user_auth.models import User


class Book(models.Model):
    id = models.AutoField(primary_key=True)
    book_id = models.CharField(max_length=200, unique=True)
    title = models.CharField(max_length=1000, blank=False, null=False)
    language = models.CharField(max_length=200, blank=True, null=True)
    infoLink = models.URLField(blank=True, null=True)
    imageLinks = models.URLField(blank=True, null=True)
    categories = models.CharField(max_length=500, blank=True, null=True)
    volumeInfo = models.URLField(blank=True, null=True)
    book_type = models.CharField(max_length=100, blank=True, null=True)
    webReaderLink = models.URLField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    authors = models.CharField(max_length=500, blank=True, null=True)
    publisher = models.CharField(max_length=500, blank=True, null=True)
    publishedDate = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return self.title


class Rating(models.Model):
    rating = models.IntegerField(verbose_name='Rating', blank=True, null=True)
    review = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)
    book = models.ForeignKey(Book, blank=False, null=False, on_delete=models.CASCADE)
    user = models.ForeignKey(User, verbose_name='User', related_name='user',
                             null=False, blank=False,
                             on_delete=models.CASCADE)

    def __str__(self):
        return self.rating


class BookReadList(models.Model):

    booklist = models.CharField(max_length=100, default="READLIST")
    book = models.ForeignKey(Book, verbose_name='Book', related_name='book',
                             null=True,
                             on_delete=models.SET_NULL)
    user = models.ForeignKey(User, verbose_name='User',
                             null=True,
                             on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now=True)

