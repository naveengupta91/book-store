from re import A
from Book_Store.settings.base import CACHE_TTL
from user_auth.serializers import UserSerializer
from user_auth.models import User
from book.models import Book, Rating, BookReadList
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .google_books import getGoogleBooks
from django.db.models import Q
from ..serializers import BookSerializer, \
       WriteRatingSerializer, WriteBookListSerializer,\
       ReadRatingSerializer, ReadBookListSerializer
from django.db.models import Sum
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Max, Count
from django.conf import Settings, settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.core.cache import cache


CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


class SearchView(APIView):
    """
        User can search any books using book title name, book author name and book id if 
        books not found then hit google book api to get search related books

    """
    def get(self, request, *args, **kwargs):

        query = request.GET.get('search')
        limit = int(request.META.get('HTTP_LIMIT', 0))
        print(limit)
        missing_field = "search word" if not query else False
        if missing_field:
            return Response({"res": "Please search any books by title, by Authors"},
                            status=status.HTTP_400_BAD_REQUEST)

        if cache.get(query):
            print("***** Fetch data from Cache ******")
            book_result = cache.get(query)
        else:
            book_result = Book.objects.filter(Q(title__icontains=query)
                                              | Q(book_id__icontains=query)
                                              | Q(authors__icontains=query))[limit:limit+20]
            if book_result:
                print("***** Hit DB  ******")
                print("***** Save data in Cache ******")
                cache.set(query, book_result[:20])

            if not book_result:
                print("******* Google book api Hit ********")
                book_result = getGoogleBooks(query)
                # cache.set(query, book_result[:20])
                return Response(book_result[:20], status=status.HTTP_200_OK)
        qs_json = BookSerializer(book_result, many=True, read_only=True)
        return Response(qs_json.data, status=status.HTTP_200_OK)


class RatingView(APIView):
    """
        User can rate and review of any books 
        :request param rating, review, user, book
        :return rating saved
    """
    def post(self, request, *args, **kwargs):
        user_id = request.data.get("user")
        book_id = request.data.get("book")
        rating = request.data.get('rating')
        missing_field = "user id" if not user_id else "book id" if \
                        not book_id else "rating" if not rating else False
        if missing_field:
            return Response({"res": f"Please pass {missing_field}"},
                            status=status.HTTP_400_BAD_REQUEST)
        serializer = WriteRatingSerializer(data=request.data)
        if serializer.is_valid():
            if int(rating) <= 5:
                try:
                    rating_instance = Rating.objects.filter(user=user_id,
                                                            book=book_id).first()
                except ObjectDoesNotExist:
                    rating_instance = None
                if rating_instance:
                    rating_instance.rating = request.data.get('rating')
                    rating_instance.review = request.data.get("review")
                    rating_instance.save()
                    return Response({"res": "Rating udpated successfully"},
                                    status=status.HTTP_200_OK)
                else:
                    serializer.save()
                    return Response({"res": "Rating saved successfully"},
                                    status=status.HTTP_200_OK)
            
            return Response({"res": "rating shoud be less than 5"},
                            status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, *args, **kwargs):
        if cache.get('top_rating'):
            print("***** Fetch data from Cache ******")
            top_rated_book = cache.get('top_rating')
        else:
            print("***** Fetch data from DB ******")
            top_rated_book = Rating.objects.values('book', 'book_id',
                                                   'book__book_id',
                                                   'book__title',
                                                   'book__authors',
                                                   'book__imageLinks')\
                            .order_by('book').annotate(count=Sum('rating')).order_by('-count')[:10]
            print(top_rated_book, 'book.')
            cache.set('top_rating', top_rated_book)
            if not top_rated_book:
                return Response({"res": "Rated books not found"}, status=status.HTTP_404_NOT_FOUND)        
        return Response(top_rated_book, status=status.HTTP_200_OK)


class SingleBookView(APIView):
    """
        Using Book is parameter we can get book details 
        :request param book_id, user_id
        :return book details 
    """
    def get(self, request, *args, **kwargs):
        book_id = request.GET.get('book_id')
        user_id = request.GET.get('user_id')
        missing_field = "user_id" if not user_id else "book_id" if \
                        not book_id else False
        if missing_field:
            return Response({"res": f"Please pass parameter {missing_field}"},
                            status=status.HTTP_400_BAD_REQUEST)
        if cache.get(book_id):
            print("***** Fetch data from Cache *****")
            data = cache.get(book_id)
        else:
            print("***** Fetch data from DB *****")
            try:
                book_instance = Book.objects.get(id=book_id)
            except ObjectDoesNotExist:
                book_instance = None
                return Response({"res": "Book does not exists"},
                                status=status.HTTP_404_NOT_FOUND)
            try:
                user_instance = User.objects.get(id=user_id)
            except ObjectDoesNotExist:
                user_instance = None
                return Response({"res": "User does not exists"},
                                status=status.HTTP_404_NOT_FOUND)
        
            rating_instance = Rating.objects.filter(book=book_id).order_by('rating')
            rating_data = ReadRatingSerializer(rating_instance, many=True,
                                               read_only=True)
            user_data = UserSerializer(user_instance)
            book_data = BookSerializer(book_instance, read_only=True)
            
            data = {"user_data": user_data.data,
                    "book_data": book_data.data,
                    "rating_data": rating_data.data[0]}
            cache.set(book_id, data)
        return Response(data, status=status.HTTP_200_OK)


class BookList(APIView):
    """
        If user want to read some books after some time so they can add in 
        read list, wishlist etc
        :request param user_id, book_id
        :return successfully added
    """
    def post(self, request, *args, **kwargs):
        user_id = request.data.get("user")
        book_id = request.data.get("book")
        missing_field = "user_id" if not user_id else "book_id" if not \
                        book_id else False
        if missing_field:
            return Response({"res": f"Kindly pass {missing_field}"},
                            status=status.HTTP_400_BAD_REQUEST)
        serializer = WriteBookListSerializer(data=request.data)
        if serializer.is_valid():
            book_instance = BookReadList.objects.filter(book=book_id,
                                                        user=user_id)
            if not book_instance:
                serializer.save()
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response({"res": "Book already in list "},
                            status=status.HTTP_409_CONFLICT)
        return Response(serializer.errors,
                        status=status.HTTP_400_BAD_REQUEST)

    """
        If user is available for read the book so they get the list of books 
        those who added in readlist so they can
        see all list data 
        :request param user_id, book_list
        :return list of data
    """
   
    def get(self, reqeust, *args, **kwargs):
        user_id = reqeust.GET.get("user_id")
        book_list = reqeust.GET.get("book_list")

        missing_field = "user_id" if not user_id else "book_list " \
                        if not book_list else False
        if missing_field:
            return Response({"res": f"Kindly pass {missing_field}"},
                            status=status.HTTP_400_BAD_REQUEST)

        booklist_instance = BookReadList.objects.filter(user=user_id,
                                                        booklist=book_list).select_related('book')
        if booklist_instance:
            list_data = ReadBookListSerializer(booklist_instance,
                                               many=True,
                                               read_only=True)
            return Response(list_data.data, status=status.HTTP_200_OK)

        return Response({"res": "Book list not found"},
                        status=status.HTTP_404_NOT_FOUND)


class GetTopReadList(APIView):
    """[THe top 10 Read list books which are maximum time readed ]
    Args:
        APIView ([GET]): [its return 10 books of records which are top
        readed books]
    """
    def get(self, reqeust, *args, **kwargs):
        booklist_instance = BookReadList.objects.order_by('-book').\
                            distinct('book_id')
        if booklist_instance:
            list_data = ReadBookListSerializer(booklist_instance,
                                               many=True,
                                               read_only=True)
            return Response(list_data.data, status=status.HTTP_200_OK)

        return Response({"res": "Book list not found"},
                        status=status.HTTP_404_NOT_FOUND)
