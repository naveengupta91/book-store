
import json
import sys
import json
from googleapiclient.discovery import build
from book.models import Book
from Book_Store.settings.base import API_KEY

"""
    Using Gooble books api we are fetching search based books and details 
"""


def getGoogleBooks(search):
    service = build('books', 'v1', developerKey=API_KEY)
    request = service.volumes().list(maxResults=40,
                                     filter='full', q={search})
    response = request.execute()
    # print(json.dumps(response, sort_keys=True, indent=4))
    # print("Number of books in list:", len(response['items']))
    res = []
    for book in response.get('items', []):
        json_data = {
            'book_id': book['id'],
            'title': book['volumeInfo']['title'] if book.get('volumeInfo').get('title') else None,
            'authors': book['volumeInfo']['authors'][0] if
            book.get('volumeInfo').get('authors') else None,
            'publisher': book['volumeInfo']['publisher'] if
            book.get('volumeInfo').get('publisher') else None,
            'publishedDate': book['volumeInfo']['publishedDate']
            if book.get('volumeInfo').get('publishedDate') else None,
            'description': book['volumeInfo']['description']
            if book.get('volumeInfo').get('description') else None,
            'language': book['volumeInfo']['language']
            if book.get('volumeInfo').get('language') else None,
            'infoLink': book['volumeInfo']['infoLink']
            if book.get('volumeInfo').get('infoLink') else None,
            'imageLinks': book['volumeInfo']['imageLinks']['thumbnail']
            if book.get('volumeInfo').get('imageLinks').get('thumbnail')
            else None,
            'categories': book['volumeInfo']['categories'][0]
            if book.get('volumeInfo').get('categories') else None,
            'volumeInfo': book['volumeInfo']['canonicalVolumeLink']
            if book.get('volumeInfo').get('canonicalVolumeLink') else None,
            'book_type': book.get('kind') if book.get('kind') else None,
            'webReaderLink': book['accessInfo']['webReaderLink']
            if book.get('accessInfo').get('webReaderLink') else None
           }
        res.append(json_data)
    createNewBook(res)

    return res

"""
    After fetched books data from google book api then call craetenewbook api which save all data in database
"""


def createNewBook(data):
    for book in data:
        book_instance = Book.objects.filter(book_id=book.get('book_id')
                                            ).order_by('id')
        if not book_instance:
            try:
                book_object = Book(book_id=book.get('book_id'),
                                   title=book.get('title'),
                                   authors=book.get('authors'),
                                   publisher=book.get("publisher"),
                                   publishedDate=book.get("publishedDate"),
                                   description=book.get('description'),
                                   language=book.get('language'),
                                   infoLink=book.get('infoLink'),
                                   imageLinks=book.get('imageLinks'),
                                   categories=book.get('volumeInfo'),
                                   volumeInfo=book.get('canonicalVolumeLink'),
                                   book_type=book.get('kind'),
                                   webReaderLink=book.get('webReaderLink'))

                book_object.save()
            except Exception as e:
                print(e)
                continue
    
    return True


