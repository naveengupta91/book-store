from django.urls import path
from .views.book_api import SearchView, RatingView, SingleBookView, BookList

urlpatterns = [
    path('search/', SearchView.as_view(), name='search'),
    path('rating/', RatingView.as_view(), name='rating'),
    path('single_book/', SingleBookView.as_view(), name='single_book'),
    path('booklist/', BookList.as_view(), name='booklist'),
]
